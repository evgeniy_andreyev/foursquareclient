//
//  ExploreModelTests.swift
//  FourSquareMVCTests
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import XCTest
import CoreLocation
@testable import FourSquareMVC

final class ExploreModelTests: XCTestCase {
    
    private var apiClientMock: APIClientMock!
    private var userLocationProviderMock: UserLocationProviderMock!
    private var lastLocationListener: ((Result<CLLocationCoordinate2D, UserLocationProvider.Error>) -> Void)!
    private var model: ExploreViewController.Model!
    
    override func setUp() {
        apiClientMock = APIClientMock()
        userLocationProviderMock = UserLocationProviderMock()
        userLocationProviderMock.implementation.onNextReceivedUserLocation = {
            self.lastLocationListener = $0
        }
        model = .init(apiClient: apiClientMock, userLocationProvider: userLocationProviderMock)
    }
    
    override func tearDown() {
        model = nil
        userLocationProviderMock = nil
        apiClientMock = nil
        lastLocationListener = nil
    }
    
    func testLoadVenuesSuccess() {
        // given
        let expectedVenues = [testVenue(with: "1"), testVenue(with: "2"), testVenue(with: "3")]
        stubExploreAPIRequest(with: expectedVenues)
        
        var receivedVenues: [Venue] = []
        model.didUpdateVenues = {
            receivedVenues = $0
        }
        
        // when
        model.loadVenuesForUserLocation()
        lastLocationListener(.success(CLLocationCoordinate2D()))
        
        // then
        XCTAssertEqual(expectedVenues.map { $0.id }, receivedVenues.map { $0.id })
    }
    
    func testSearchVenuesSuccess() {
        // given
        let expectedVenues = [testVenue(with: "1"), testVenue(with: "2"), testVenue(with: "3")]
        stubSearchAPIRequest(with: expectedVenues)
        
        var receivedVenues: [Venue] = []
        model.didUpdateVenues = {
            receivedVenues = $0
        }
        
        // when
        model.search(in: MapBoundingBox(northEast: .init(), southWest: .init()))
        
        // then
        XCTAssertEqual(expectedVenues.map { $0.id }, receivedVenues.map { $0.id })
    }
    
    func testCancelsOutSubsequentLoadings() {
        // given
        let expectedVenues = [testVenue(with: "1"), testVenue(with: "2"), testVenue(with: "3")]
        apiClientMock.callbackExecutionDelay = 0.1
        stubSearchAPIRequest(with: expectedVenues)
        
        let expectation = self.expectation(description: "Did update callback was called")
        expectation.assertForOverFulfill = true
        expectation.expectedFulfillmentCount = 1
        
        model.didUpdateVenues = { _ in
            expectation.fulfill()
        }
        
        // when
        model.search(in: MapBoundingBox(northEast: .init(), southWest: .init()))
        model.search(in: MapBoundingBox(northEast: .init(), southWest: .init()))
        
        // then
        wait(for: [expectation], timeout: apiClientMock.callbackExecutionDelay * 3)
    }
    
    func testNotifiesOnLocationError() {
        // given
        let expectedVenues = [testVenue(with: "1"), testVenue(with: "2"), testVenue(with: "3")]
        stubExploreAPIRequest(with: expectedVenues)
        
        var receivedLocationError: ExploreViewController.Model.Error?
        model.didReceiveError = {
            receivedLocationError = $0
        }
        var receivedVenues: [Venue] = []
        model.didUpdateVenues = {
            receivedVenues = $0
        }
        
        // when
        model.loadVenuesForUserLocation()
        lastLocationListener(.failure(.undefined))
        
        // then
        XCTAssertNotNil(receivedLocationError)
        XCTAssertTrue(receivedVenues.isEmpty)
    }
    
    func testNotifiesOnNetworkError() {
        // given
        stubbAPIRequestWithError()
        
        var receivedNetworkError: ExploreViewController.Model.Error?
        model.didReceiveError = {
            receivedNetworkError = $0
        }
        
        // when
        model.loadVenuesForUserLocation()
        lastLocationListener(.success(CLLocationCoordinate2D()))
        
        // then
        XCTAssertNotNil(receivedNetworkError)
    }
    
}

// MARK: Helpers

private extension ExploreModelTests {
    
    func testVenue(with id: String) -> Venue {
        return Venue(id: id, name: "", location: .init(latitude: 0, longitude: 0, formattedAddress: []))
    }
    
    func stubExploreAPIRequest(with venues: [Venue]) {
        let expectedResponse = ExploreRequest.Response(groups: [
            .init(items: venues.map(ExploreResponse.Group.Item.init))
        ])
        let expectedResult = ExploreRequest.ResultType(response: expectedResponse)
        let encodedResult = try! JSONEncoder().encode(expectedResult)
        apiClientMock.stubbedDataResult = .success(encodedResult)
    }
    
    func stubSearchAPIRequest(with venues: [Venue]) {
        let expectedResponse = SearchRequest.Response(venues: venues)
        let expectedResult = SearchRequest.ResultType(response: expectedResponse)
        let encodedResult = try! JSONEncoder().encode(expectedResult)
        apiClientMock.stubbedDataResult = .success(encodedResult)
    }
    
    func stubbAPIRequestWithError() {
        struct TestError: Error {}
        
        apiClientMock.stubbedDataResult = .failure(TestError())
    }
    
}

//
//  RootCoordinatorTests.swift
//  FourSquareMVCTests
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import XCTest
@testable import FourSquareMVC

final class RootCoordinatorTests: XCTestCase {
    
    private var window: UIWindow!
    private var coordinator: RootCoordinator!
    
    override func setUp() {
        window = UIWindow()
        coordinator = RootCoordinator(window: window,
                                      dependencies: .init(apiClient: APIClientMock(),
                                                          userLocationProvider: UserLocationProviderMock()))
    }
    
    override func tearDown() {
        coordinator = nil
        window = nil
    }
    
    func testShowsExploreOnStart() {
        XCTAssertFalse(window.rootViewController is ExploreViewController)
        
        coordinator.start()
        
        XCTAssertTrue(window.rootViewController is ExploreViewController)
    }
    
    func testShowsDetailsOnVenueSelection() {
        // given
        coordinator.start()
        let exploreViewController: ExploreViewController! = window.rootViewController as? ExploreViewController
        
        // when
        UIView.performWithoutAnimation {
            exploreViewController.didSelectVenue(testVenue())
        }
        
        // then
        XCTAssertTrue(window.rootViewController?.presentedViewController is VenueDetailViewController)
    }
    
}

private extension RootCoordinatorTests {
    
    func testVenue() -> Venue {
        return Venue(id: "Test", name: "Test", location: .init(latitude: 0, longitude: 0, formattedAddress: []))
    }
    
}

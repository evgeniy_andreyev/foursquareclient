//
//  APIClientTests.swift
//  FourSquareMVCTests
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import XCTest
@testable import FourSquareMVC

final class APIClientTests: XCTestCase {
    
    private var requestExecutorMock: RequestExecutorMock!
    private var apiClient: APIClient!
    
    override func setUp() {
        requestExecutorMock = RequestExecutorMock()
        apiClient = APIClientImpl(requestExecutor: requestExecutorMock)
    }
    
    override func tearDown() {
        apiClient = nil
        requestExecutorMock = nil
    }
    
    func testFailsOnExecutorError() {
        // given
        struct TestError: Error {}
        stubRequest(with: .failure(TestError()))
        
        // when
        var actualResult: Result<TestEntity, Error>!
        apiClient.fetch(request: TestRequest()) { result in
            actualResult = result
        }
        
        // then
        var actualError: Error?
        if case .failure(let error) = actualResult {
            actualError = error
        }
        
        XCTAssertTrue(actualError is TestError)
    }
    
    func testSuccessfulRequest() {
        // given
        let expectedResponse = TestEntity()
        let expectedResult = RequestResult(response: expectedResponse)
        let encodedResult: Data! = try? JSONEncoder().encode(expectedResult)
        stubRequest(with: .success(encodedResult))
        
        // when
        var actualResult: Result<TestEntity, Error>!
        apiClient.fetch(request: TestRequest()) { result in
            actualResult = result
        }
        
        // then
        XCTAssertEqual(try? actualResult.get(), expectedResponse)
    }
    
}

// MARK: Stubs
private extension APIClientTests {
    
    func stubRequest(with result: Result<Data, Error>) {
        requestExecutorMock.implementation.executeRequest = { _, handler in
            handler(result)
        }
    }
    
}

// MARK: Mocks and fixtures
private extension APIClientTests {
    
    struct TestEntity: Codable, Equatable {
        
        var name = "Test"
        
    }
    
    struct TestRequest: APIRequest {
        
        typealias Response = TestEntity
        
        var endpoint = Endpoint(path: "test", queryItems: .init())
        
    }
    
}

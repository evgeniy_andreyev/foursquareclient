//
//  UserLocationProviderMock.swift
//  FourSquareMVCTests
//
//  Created by Eugene Andreyev on 26/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation
import CoreLocation
@testable import FourSquareMVC

final class UserLocationProviderMock: UserLocationProviding {
    
    struct Implementation {
        
        var isLocationUsageAuthorized: () -> Bool = { return true }
        var requestUserLocation: () -> Void = {}
        var onNextReceivedUserLocation: (@escaping (Result<CLLocationCoordinate2D, UserLocationProvider.Error>) -> Void) -> Void = { _ in }
        
    }
    
    var implementation = Implementation()
    
    var isLocationUsageAuthorized: Bool {
        return implementation.isLocationUsageAuthorized()
    }
    
    func requestUserLocation() {
        implementation.requestUserLocation()
    }
    
    func onNextReceivedUserLocation(_ callback: @escaping (Result<CLLocationCoordinate2D, UserLocationProvider.Error>) -> Void) {
        implementation.onNextReceivedUserLocation(callback)
    }
    
    
}

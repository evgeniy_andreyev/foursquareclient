//
//  RequestExecutorMock.swift
//  FourSquareMVCTests
//
//  Created by Eugene Andreyev on 26/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation
@testable import FourSquareMVC

final class RequestExecutorMock: RequestExecutor {
    
    var implementation = Implementation()
    
    struct Implementation {
        var executeRequest: (Endpoint, @escaping (Result<Data, Error>) -> Void) -> Void = { _, _ in
            return
        }
    }
    
    func executeRequest(_ endpoint: Endpoint, handler: @escaping (Result<Data, Error>) -> Void) {
        implementation.executeRequest(endpoint, handler)
    }
    
}

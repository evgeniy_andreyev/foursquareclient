//
//  APIClientMock.swift
//  FourSquareMVCTests
//
//  Created by Eugene Andreyev on 26/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation
@testable import FourSquareMVC

final class APIClientMock: APIClient {
    
    var stubbedDataResult: Result<Data, Error>?
    var callbackExecutionDelay: TimeInterval = 0.0
    
    @discardableResult
    func fetch<Request>(request: Request,
                        handler: @escaping (Result<Request.Response, Error>) -> Void) -> Cancellable where Request: APIRequest {
        let token = CancellableToken()
        
        guard let stubbedDataResult = stubbedDataResult else {
            return token
        }
        
        let result: Result<Request.Response, Error>
        switch stubbedDataResult {
        case .success(let data):
            let stubbedResponse = try! JSONDecoder().decode(Request.ResultType.self, from: data)
            result = .success(stubbedResponse.response)
            
        case .failure(let error):
            result = .failure(error)
        }
        
        if callbackExecutionDelay > 0.001 {
            DispatchQueue.main.asyncAfter(deadline: .now() + callbackExecutionDelay) {
                guard !token.isCancelled else {
                    return
                }
                
                handler(result)
            }
        } else {
            handler(result)
        }
        
        return token
    }
    
}

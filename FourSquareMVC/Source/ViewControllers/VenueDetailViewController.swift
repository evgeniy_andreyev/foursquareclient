//
//  VenueDetailViewController.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import UIKit

final class VenueDetailViewController: UIViewController {
    
    private var typedView: VenueDetailsView! {
        return view as? VenueDetailsView
    }
    private let model: Model
    
    init(model: Model) {
        self.model = model
        
        super.init(nibName: nil, bundle: nil)
        
        setupCallbacks()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = VenueDetailsView()
    }
    
    private func setupCallbacks() {
        model.didUpdateVenueDetails = { [weak self] details in
            DispatchQueue.main.async {
                self?.typedView.venueDetails = details
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        typedView.venueDetails = model.venueDetails
        model.loadDetails()
    }
    
}

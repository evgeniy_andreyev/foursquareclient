//
//  ExploreViewController.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

final class ExploreViewController: UIViewController {
    
    var didSelectVenue: (Venue) -> Void = { _ in }
    
    private var typedView: ExploreView! {
        return view as? ExploreView
    }
    private let model: Model
    private let throttler: Throttler
    
    init(model: Model, throttler: Throttler = .init(throttleInterval: 0.5)) {
        self.model = model
        self.throttler = throttler
        
        super.init(nibName: nil, bundle: nil)
        
        setupCallbacks()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let view = ExploreView()
        view.didSelectMapItem = { [weak self] mapItem in
            self?.select(mapItem: mapItem)
        }
        view.didUpdateVisibleRegion = { [weak self] mapRegion in
            self?.search(in: mapRegion)
        }
        
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model.loadVenuesForUserLocation()
    }
    
    private func setupCallbacks() {
        let scheduleUIUpdate: (@escaping (ExploreViewController) -> Void) -> Void = { [weak self] update in
            DispatchQueue.main.async {
                self.flatMap(update)
            }
        }
        model.didUpdateVenues = { venues in
            scheduleUIUpdate {
                $0.typedView.mapItems = venues.map { MapItem(id: $0.id, coordinate: $0.location.coordinate) }
            }
        }
        model.didReceiveError = { error in
            scheduleUIUpdate {
                $0.handleError(error)
            }
        }
    }
    
}

// MARK: Data loading
private extension ExploreViewController {
    
    func select(mapItem: MapItem) {
        guard let selectedVenue = model.venues.first(where: { $0.id == mapItem.id }) else {
            assertionFailure("Model and View are not synchronised")
            return
        }
        
        didSelectVenue(selectedVenue)
    }
    
    func search(in region: MKCoordinateRegion) {
        let boundingBox = MapBoundingBox(region: region)
        throttler.throttle { [weak self] in
            self?.model.search(in: boundingBox)
        }
    }
    
}

// MARK: Alerts
private extension ExploreViewController {
    
    func handleError(_ error: Model.Error) {
        let alert: UIAlertController
        switch error {
        case .failedToLoadVenuesForUserLocation:
            alert = self.alert(withMessage: "Failed to load venues. Check your internet connection.",
                               confirmationTitle: "OK",
                               retryHandler: { [weak self] in self?.model.loadVenuesForUserLocation() })
            
        case .failedToUpdateUserLocation:
            let message = "Failed to get your location.\nPlease check setttings.\nYou are still able to browse."
            alert = self.alert(withMessage: message,
                               confirmationTitle: "I'll just browse",
                               retryHandler: { [weak self] in self?.model.loadVenuesForUserLocation() })
            
        case .failedToSearchIn(let boundingBox):
            alert = self.alert(withMessage: "Failed to load venues. Check your internet connection or zoom in on map.",
                               confirmationTitle: "OK",
                               retryHandler: { [weak self] in self?.model.search(in: boundingBox) })
        }
        
        present(alert, animated: true)
    }
     
     private func alert(withMessage message: String,
                        confirmationTitle: String,
                        retryHandler: @escaping () -> Void) -> UIAlertController {
         // TODO: Add localization
         let alert = UIAlertController(title: "Ooops",
                                       message: message,
                                       preferredStyle: .alert)
         let dismissAction = UIAlertAction(title: confirmationTitle, style: .cancel, handler: nil)
         let retryAction = UIAlertAction(title: "Try again", style: .default) { _ in
             retryHandler()
         }
         [dismissAction, retryAction].forEach(alert.addAction)
         
         return alert
     }
    
    
}

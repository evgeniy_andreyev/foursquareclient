//
//  ExploreView.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import UIKit
import MapKit

final class MapItem: NSObject, MKAnnotation {
    
    let id: String
    let coordinate: CLLocationCoordinate2D
    
    init(id: String, coordinate: CLLocationCoordinate2D) {
        self.id = id
        self.coordinate = coordinate
    }
    
}

final class ExploreView: UIView {
    
    var mapItems = [MapItem]() {
        didSet {
            mapView.removeAnnotations(oldValue)
            mapView.addAnnotations(mapItems)
        }
    }

    var didSelectMapItem: (MapItem) -> Void = { _ in }
    var didUpdateVisibleRegion: (MKCoordinateRegion) -> Void = { _ in}
    
    private let mapView = MKMapView()
    private var shouldFocusOnUserLocation = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        setupMapView()
    }
    
    private func setupMapView() {
        mapView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(mapView)
        
        NSLayoutConstraint.activate([
            mapView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mapView.topAnchor.constraint(equalTo: topAnchor),
            mapView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        
        mapView.showsUserLocation = true
        
        mapView.delegate = self
    }
    
}

private extension ExploreView {
    
    func focus(on coordinate: CLLocationCoordinate2D) {
        let viewRegion = MKCoordinateRegion(center: coordinate,
                                            latitudinalMeters: Constants.defaultRadiusMeters,
                                            longitudinalMeters: Constants.defaultRadiusMeters)
        mapView.setRegion(viewRegion, animated: false)
    }
    
}

extension ExploreView: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let mapItem = annotation as? MapItem else {
            return nil
        }
        
        let identifier = String(describing: MapItem.self)
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) ??
            MKPinAnnotationView(annotation: mapItem, reuseIdentifier: identifier)
        annotationView.annotation = mapItem
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        guard shouldFocusOnUserLocation else {
            return
        }
        
        focus(on: userLocation.coordinate)
        
        shouldFocusOnUserLocation = false
    }
    
    func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
        shouldFocusOnUserLocation = false
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let mapItem = view.annotation as? MapItem else {
            return
        }
        
        didSelectMapItem(mapItem)
    }
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        // The update is ignored until the user location request is not completed
        guard !shouldFocusOnUserLocation else {
            return
        }
        
        didUpdateVisibleRegion(mapView.region)
    }
    
}

private extension ExploreView {
    
    enum Constants {
        
        static var defaultRadiusMeters: CLLocationDistance {
            return 1000
        }
        
    }
    
}

//
//  VenueDetailsView.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import UIKit

final class VenueDetailsView: UIView {
    
    var venueDetails: VenueDetails? {
        didSet {
            UIView.animate(withDuration: 0.3) {
                self.updateUI()
            }
        }
    }
    
    private let contentView = UIStackView()
    private let nameView = TextCell(title: "Name")
    private let addressView = TextCell(title: "Address")
    private let aboutView = TextCell(title: "About")
    private let hoursView = TextCell(title: "Hours")
    private let contactsView = TextCell(title: "Contacts")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        backgroundColor = .white
        
        setupContentView()
        contentView.addArrangedSubview(nameView)
        contentView.addArrangedSubview(addressView)
        contentView.addArrangedSubview(aboutView)
        contentView.addArrangedSubview(hoursView)
        contentView.addArrangedSubview(contactsView)

        updateUI()
    }
    
    private func setupContentView() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(contentView)
        
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor),
        ])
        
        contentView.axis = .vertical
        contentView.distribution = .fill
        contentView.alignment = .fill
        contentView.spacing = 8
    }
    
    private func updateUI() {
        nameView.body = venueDetails?.name
        addressView.body = venueDetails?.location.addressText
        
        func updateOptionalField(_ field: String?, cell: TextCell) {
            cell.body = field
            cell.isHidden = field.flatMap { $0.isEmpty } ?? true
        }
        
        updateOptionalField(venueDetails?.description, cell: aboutView)
        updateOptionalField(venueDetails?.hours?.status, cell: hoursView)
        updateOptionalField(venueDetails?.contact?.formattedPhone, cell: contactsView)
    }
    
}

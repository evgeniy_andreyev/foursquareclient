//
//  TextCell.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import UIKit

final class TextCell: UIView {
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    var body: String? {
        didSet {
            bodyLabel.text = body
        }
    }
    
    private let titleLabel = UILabel()
    private let bodyLabel = UILabel()
    
    init(frame: CGRect = .zero, title: String? = nil) {
        super.init(frame: frame)
        
        setup()
        
        titleLabel.text = title
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        setupTitleLabel()
        setupBodyLabel()
    }
    
    private func setupTitleLabel() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalPadding),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalPadding),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Constants.verticalPadding),
            titleLabel.heightAnchor.constraint(equalToConstant: 30),
        ])
        
        titleLabel.font = UIFont.systemFont(ofSize: 25, weight: .black)
    }
    
    private func setupBodyLabel() {
        bodyLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bodyLabel)
        
        NSLayoutConstraint.activate([
            bodyLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalPadding),
            bodyLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalPadding),
            bodyLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Constants.interItemSpacing),
            bodyLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.verticalPadding),
            bodyLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 18),
        ])
        bodyLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        
        bodyLabel.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        bodyLabel.numberOfLines = 3
    }
    
}

private extension TextCell {
    
    enum Constants {
        
        static var horizontalPadding: CGFloat {
            return 18
        }
        
        static var verticalPadding: CGFloat {
            return 8
        }
        
        static var interItemSpacing: CGFloat {
            return 12
        }
        
    }
    
}

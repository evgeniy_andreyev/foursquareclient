//
//  VenueDetailsModel.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 25/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//


extension VenueDetailViewController {
    
    final class Model {
        
        private(set) var venueDetails: VenueDetails {
            didSet {
                didUpdateVenueDetails(venueDetails)
            }
        }
        var didUpdateVenueDetails: (VenueDetails) -> Void = { _ in }
        
        private let venue: Venue
        private let apiClient: APIClient
        
        init(apiClient: APIClient = APIClientImpl(),
             venue: Venue) {
            self.venue = venue
            self.apiClient = apiClient
            self.venueDetails = VenueDetails(venue: venue)
        }
        
        func loadDetails() {
            apiClient.fetch(request: VenueDetailsRequest(venue: venue)) { [weak self] result in
                guard let details = try? result.get().venue else {
                    return
                }
                
                self?.venueDetails = details
            }
        }
        
    }
    
}

//
//  ExploreModel.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation
import CoreLocation

extension ExploreViewController {
    
    final class Model {
        
        enum Error: Swift.Error {
            case failedToUpdateUserLocation
            case failedToLoadVenuesForUserLocation
            case failedToSearchIn(MapBoundingBox)
        }
        
        var didUpdateVenues: (([Venue]) -> Void)?
        var didReceiveError: ((Error) -> Void)?
        private(set) var venues = [Venue]() {
            didSet {
                didUpdateVenues?(self.venues)
            }
        }
        
        private let apiClient: APIClient
        private let userLocationProvider: UserLocationProviding
        private var currentRequestToken: Cancellable? {
            didSet {
                oldValue?.cancel()
            }
        }
        
        init(apiClient: APIClient = APIClientImpl(),
             userLocationProvider: UserLocationProviding = UserLocationProvider()) {
            self.apiClient = apiClient
            self.userLocationProvider = userLocationProvider
        }
        
        func loadVenuesForUserLocation() {
            userLocationProvider.requestUserLocation()
            userLocationProvider.onNextReceivedUserLocation { [weak self] result in
                switch result {
                case .success(let coordinate):
                    self?.onReceivedUserLocation(coordinate: coordinate)
                case .failure:
                    self?.didReceiveError?(.failedToUpdateUserLocation)
                }
            }
        }
        
        func search(in boundingBox: MapBoundingBox) {
            let request = SearchRequest(boundingBox: boundingBox)
            currentRequestToken = apiClient.fetch(request: request) { [weak self] result in
                switch result {
                case .success(let response):
                    self?.venues = response.venues
                case .failure:
                    self?.didReceiveError?(.failedToSearchIn(boundingBox))
                }
            }
        }
        
        private func onReceivedUserLocation(coordinate: CLLocationCoordinate2D) {
            let request = ExploreRequest(location: coordinate)
            currentRequestToken = apiClient.fetch(request: request) { [weak self] result in
                switch result {
                case .success(let response):
                    self?.venues = response.venues
                case .failure:
                    self?.didReceiveError?(.failedToLoadVenuesForUserLocation)
                }
            }
        }
        
    }
    
}

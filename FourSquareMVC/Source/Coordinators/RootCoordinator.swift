//
//  RootCoordinator.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import UIKit
import CoreLocation

final class RootCoordinator {
    
    struct Dependencies {
        
        var apiClient: APIClient = APIClientImpl()
        var userLocationProvider: UserLocationProviding = UserLocationProvider()
        
    }
    
    private let window: UIWindow
    private let dependencies: Dependencies
    
    init(window: UIWindow, dependencies: Dependencies = .init()) {
        self.window = window
        self.dependencies = dependencies
    }
    
    func start() {
        window.rootViewController = createRootController()
        window.makeKeyAndVisible()
    }
    
    private func createRootController() -> UIViewController {
        let model = ExploreViewController.Model(apiClient: dependencies.apiClient,
                                                userLocationProvider: dependencies.userLocationProvider)
        let controller = ExploreViewController(model: model)
        controller.didSelectVenue = { [weak self] venue in
            self?.showDetail(for: venue)
        }
        return controller
    }
    
    private func showDetail(for venue: Venue) {
        let model = VenueDetailViewController.Model(apiClient: dependencies.apiClient, venue: venue)
        let controller = VenueDetailViewController(model: model)
        window.rootViewController?.present(controller, animated: true)
    }
    
}

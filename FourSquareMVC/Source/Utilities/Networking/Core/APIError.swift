//
//  APIError.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

enum APIError: Error {
    
    case malformedURL
    case networking(Error?)
    
}

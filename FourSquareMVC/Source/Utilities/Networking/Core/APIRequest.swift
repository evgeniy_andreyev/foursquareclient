//
//  APIRequest.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

struct RequestResult<Response: Codable>: Codable {
    
    let response: Response
    
}

protocol APIRequest {
    
    associatedtype Response: Codable
    
    typealias ResultType = RequestResult<Response>

    var endpoint: Endpoint { get }
    
}

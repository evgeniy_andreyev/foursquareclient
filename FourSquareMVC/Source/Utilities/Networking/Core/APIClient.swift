//
//  APIClient.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

protocol APIClient {
    
    @discardableResult
    func fetch<Request: APIRequest>(request: Request,
                                    handler: @escaping (Result<Request.Response, Error>) -> Void) -> Cancellable
    
}

class APIClientImpl: APIClient {
    
    private let requestExecutor: RequestExecutor
    
    init(requestExecutor: RequestExecutor = URLSessionRequestExecutor()) {
        self.requestExecutor = requestExecutor
    }
    
    @discardableResult
    func fetch<Request: APIRequest>(request: Request,
                                    handler: @escaping (Result<Request.Response, Error>) -> Void) -> Cancellable {
        let token = CancellableToken()
        defer {
                requestExecutor.executeRequest(request.endpoint) { [weak token, weak self] result in
                    if let token = token, token.isCancelled {
                        return
                    }
                    
                    self?.digestResult(result, for: request, handler: handler)
                }
        }
        
        return token
    }
    
    private func digestResult<Request: APIRequest>(_ result: Result<Data, Error>,
                                                   for request: Request,
                                                   handler: @escaping (Result<Request.Response, Error>) -> Void) {
        switch result {
        case .success(let data):
            do {
                let decodedResult = try JSONDecoder().decode(Request.ResultType.self, from: data)
                handler(.success(decodedResult.response))
            } catch(let error) {
                handler(.failure(error))
            }
            
        case .failure(let error):
            handler(.failure(error))
        }
    }
    
}

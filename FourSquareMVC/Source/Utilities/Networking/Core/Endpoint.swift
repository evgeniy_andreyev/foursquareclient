//
//  Endpoint.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

struct Endpoint {
    
    let path: String
    let queryItems: [URLQueryItem]
    
}

extension Endpoint {
    
    var url: URL? {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.foursquare.com"
        components.path = "/v2" + path
        components.queryItems = authorizationItems + [versionItem] + queryItems

        return components.url
    }
    
    private var authorizationItems: [URLQueryItem] {
        precondition(!Environment.clientSecret.isEmpty && !Environment.clientID.isEmpty,
                     "Set clientID and clientSecret in Environment constants")
        return [
            URLQueryItem(name: "client_id", value: Environment.clientID),
            URLQueryItem(name: "client_secret", value: Environment.clientSecret),
        ]
    }
    
    private var versionItem: URLQueryItem {
        return URLQueryItem(name: "v", value: "20180323")
    }
    
}

private extension Endpoint {
    
    enum Environment {
    
        static var clientID: String {
            return ""
        }
        
        static var clientSecret: String {
            return ""
        }
        
    }
    
}

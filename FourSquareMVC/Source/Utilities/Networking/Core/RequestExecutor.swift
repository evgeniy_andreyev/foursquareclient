//
//  RequestExecutor.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

protocol RequestExecutor {
    
    func executeRequest(_ endpoint: Endpoint, handler: @escaping (Result<Data, Error>) -> Void)
    
}

final class URLSessionRequestExecutor: RequestExecutor {
    
    private let urlSession: URLSession
    
    init(urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }
    
    func executeRequest(_ endpoint: Endpoint, handler: @escaping (Result<Data, Error>) -> Void) {
        guard let url = endpoint.url else {
            return handler(.failure(APIError.malformedURL))
        }

        let task = urlSession.dataTask(with: url) { data, response, error in
            guard let data = data,
                let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode) else {
                    
                    handler(.failure(APIError.networking(error)))
                return
            }

            handler(.success(data))
        }

        task.resume()
    }
    
}

//
//  VenueDetailsRequest.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

extension Endpoint {
    
    static func details(venue: Venue) -> Endpoint {
        return Endpoint(path: "/venues/\(venue.id)", queryItems: [])
    }
    
}

struct VenueDetailsResponse: Codable {
    
    let venue: VenueDetails
    
}

struct VenueDetailsRequest: APIRequest {
    
    typealias Response = VenueDetailsResponse
    
    let endpoint: Endpoint
    
    init(venue: Venue) {
        self.endpoint = .details(venue: venue)
    }
    
}

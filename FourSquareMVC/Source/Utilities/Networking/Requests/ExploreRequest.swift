//
//  ExploreRequest.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation
import struct CoreLocation.CLLocationCoordinate2D

extension Endpoint {
    
    static func explore(location: CLLocationCoordinate2D) -> Endpoint {
        return Endpoint(path: "/venues/explore",
                        queryItems: [
                            URLQueryItem(name: "v", value: "20180323"),
                            URLQueryItem(name: "ll", value: "\(location.latitude),\(location.longitude)"),
                            URLQueryItem(name: "limit", value: "20"),
                            URLQueryItem(name: "categoryId", value: "4d4b7105d754a06374d81259")
        ])
    }
    
}

struct ExploreResponse: Codable {
    
    struct Group: Codable {
        
        struct Item: Codable {
            
            let venue: Venue
            
        }
        
        let items: [Item]
        
    }
    
    let groups: [Group]
    
    var venues: [Venue] {
        return groups.flatMap { $0.items.compactMap { $0.venue } }
    }
    
}

struct ExploreRequest: APIRequest {
    
    typealias Response = ExploreResponse
    
    var endpoint: Endpoint {
        return .explore(location: location)
    }
    private let location: CLLocationCoordinate2D
    
    init(location: CLLocationCoordinate2D) {
        self.location = location
    }
    
}

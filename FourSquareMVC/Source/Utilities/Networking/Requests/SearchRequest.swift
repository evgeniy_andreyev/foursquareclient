//
//  SearchRequest.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 25/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

extension Endpoint {
    
    static func search(boundingBox: MapBoundingBox) -> Endpoint {
        return Endpoint(path: "/venues/search",
                        queryItems: [
                            URLQueryItem(name: "v", value: "20180323"),
                            URLQueryItem(name: "limit", value: "20"),
                            URLQueryItem(name: "categoryId", value: "4d4b7105d754a06374d81259"),
                            URLQueryItem(name: "intent", value: "browse"),
                            URLQueryItem(name: "sw", value: "\(boundingBox.southWest.latitude),\(boundingBox.southWest.longitude)"),
                            URLQueryItem(name: "ne", value: "\(boundingBox.northEast.latitude),\(boundingBox.northEast.longitude)"),
        ])
    }
    
}

struct SearchResponse: Codable {
    
    var venues: [Venue]
    
}

struct SearchRequest: APIRequest {
    
    typealias Response = SearchResponse
    
    let endpoint: Endpoint
    
    init(boundingBox: MapBoundingBox) {
        self.endpoint = .search(boundingBox: boundingBox)
    }
    
}

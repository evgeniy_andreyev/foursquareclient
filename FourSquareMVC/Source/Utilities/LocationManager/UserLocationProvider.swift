//
//  UserLocationProvider.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManager: class {
    
    var delegate: CLLocationManagerDelegate? { get set }
    var isLocationUsageAuthorized: Bool { get }
    
    func requestAuthorizationIfNeeded()
    func requestLocation()
    
}

extension CLLocationManager: LocationManager {
    
    var isLocationUsageAuthorized: Bool {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        return authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse
    }
    
    func requestAuthorizationIfNeeded() {
        guard CLLocationManager.authorizationStatus() == .notDetermined else {
            return
        }
        
        requestWhenInUseAuthorization()
    }
    
}

protocol UserLocationProviding {
    
    var isLocationUsageAuthorized: Bool { get }
    
    func requestUserLocation()
    func onNextReceivedUserLocation(_ callback: @escaping (Result<CLLocationCoordinate2D, UserLocationProvider.Error>) -> Void)
    
}

class UserLocationProvider: NSObject, UserLocationProviding {
    
    enum Error: Swift.Error {
        
        case notAuthorized, undefined
        
    }
    
    var isLocationUsageAuthorized: Bool {
        return locationManager.isLocationUsageAuthorized
    }
    
    private let locationManager: LocationManager
    
    private var userLocationCoordinate: CLLocationCoordinate2D?
    private var userLocationListeners: [(Result<CLLocationCoordinate2D, Error>) -> Void] = []
    
    init(locationManager: LocationManager = CLLocationManager()) {
        self.locationManager = locationManager
        
        super.init()
        
        locationManager.delegate = self
    }
    
    func requestUserLocation() {
        locationManager.requestAuthorizationIfNeeded()
        locationManager.requestLocation()
    }
    
    func onNextReceivedUserLocation(_ callback: @escaping (Result<CLLocationCoordinate2D, Error>) -> Void) {
        if let userLocationCoordinate = userLocationCoordinate {
            callback(.success(userLocationCoordinate))
        } else {
            userLocationListeners.append(callback)
        }
    }
    
    private func notifyListeners(with result: Result<CLLocationCoordinate2D, Error>) {
        let listeners = self.userLocationListeners
        self.userLocationListeners = []
        listeners.forEach { $0(result) }
    }
    
}

extension UserLocationProvider: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        userLocationCoordinate = location.coordinate
        
        notifyListeners(with: .success(location.coordinate))
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Swift.Error) {
        notifyListeners(with: .failure(isLocationUsageAuthorized ? .undefined : .notAuthorized))
    }
    
}

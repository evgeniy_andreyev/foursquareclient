//
//  CancellableToken.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 26/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

/// A token which represents cancellation state of cancellable operation.
/// An implementation must be thread-safe.
protocol Cancellable {
    
    var isCancelled: Bool { get }
    
    func cancel()
    
}

final class CancellableToken: Cancellable {
    
    private var lock = os_unfair_lock_s()
    private var _isCancelled = false
    
    var isCancelled: Bool {
        os_unfair_lock_lock(&lock)
        defer { os_unfair_lock_unlock(&lock) }
        
        return _isCancelled
    }
    
    func cancel() {
        os_unfair_lock_lock(&lock)
        defer { os_unfair_lock_unlock(&lock) }
        
        guard !_isCancelled else {
            return
        }
        
        _isCancelled = true
    }
    
}

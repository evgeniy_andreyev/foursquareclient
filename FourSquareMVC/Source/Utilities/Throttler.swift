//
//  Throttler.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 25/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import Foundation

/// Schedules task on the main thread with throttling.
/// Each subsequent task scheduled earlier than throttle interval expired, would cancel out the previous task.
/// This class is not thread-safe, so all calls to it should be performed within the same thread.
final class Throttler {
 
    private let throttleInterval: TimeInterval
    private var currentTask: DispatchWorkItem?
 
    init(throttleInterval: TimeInterval) {
        self.throttleInterval = throttleInterval
    }
 
    /// Schedules the specified block after the throttling interval on the main thread.
    /// The block is called if the throttle function wasn't called for `interval` seconds.
    func throttle(_ block: @escaping () -> Void) {
        currentTask?.cancel()
 
        let nextTask = DispatchWorkItem(block: block)
        currentTask = nextTask
 
        DispatchQueue.main.asyncAfter(deadline: .now() + throttleInterval,
                                      execute: nextTask)
    }
 
}

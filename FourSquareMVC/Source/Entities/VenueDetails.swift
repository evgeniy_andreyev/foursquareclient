//
//  VenueDetails.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

struct VenueDetails: Codable {
    
    struct Hours: Codable {
        
        let status: String
        
    }
    
    struct Contact: Codable {
        
        let formattedPhone: String?
        
    }
    
    let id: String
    let name: String
    let location: VenueLocation
    let rating: Double?
    let description: String?
    let hours: Hours?
    let contact: Contact?
    
}

extension VenueDetails {
    
    init(venue: Venue) {
        self.id = venue.id
        self.name = venue.name
        self.location = venue.location
        self.rating = nil
        self.description = nil
        self.hours = nil
        self.contact = nil
    }
    
}

//
//  MapBoundingBox.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 25/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import MapKit

struct MapBoundingBox {
    
    let northEast: CLLocationCoordinate2D
    let southWest: CLLocationCoordinate2D
    
}

extension MapBoundingBox {
    
    init(region: MKCoordinateRegion) {
        let north = region.center.latitude + (region.span.latitudeDelta / 2)
        let east = region.center.longitude + (region.span.longitudeDelta / 2)
        northEast = CLLocationCoordinate2D(latitude: north, longitude: east)
        let south = region.center.latitude - (region.span.latitudeDelta / 2)
        let west = region.center.longitude - (region.span.longitudeDelta / 2)
        southWest = CLLocationCoordinate2D(latitude: south, longitude: west)
    }
}

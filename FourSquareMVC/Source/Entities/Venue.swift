//
//  Venue.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 23/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

struct Venue: Codable {
    
    let id: String
    let name: String
    let location: VenueLocation
    
}

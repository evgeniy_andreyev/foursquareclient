//
//  VenueLocation.swift
//  FourSquareMVC
//
//  Created by Eugene Andreyev on 24/11/2019.
//  Copyright © 2019 Eugene Andreyev. All rights reserved.
//

import struct CoreLocation.CLLocationCoordinate2D

struct VenueLocation: Codable {
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
        case formattedAddress
    }
    
    let latitude: Double
    let longitude: Double
    let formattedAddress: [String]
    var addressText: String {
        return formattedAddress.joined(separator: "\n")
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
}
